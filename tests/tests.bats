setup() {
  load ".env"
  load "${BATS_HELPER_PATH}/bats-support/load"
  load "${BATS_HELPER_PATH}/bats-assert/load"
}

teardown() {
  unset BATS_HELPER_PATH
}

@test "Can run our script" {
  run ./wakeup.sh
  assert_output 'Debout Néo'
}
